# {{ cookiecutter.project_name }}

[![pipeline status](https://{{ cookiecutter.git_url }}/{{ cookiecutter.git_group }}/{{ cookiecutter.project_slug }}/badges/main/pipeline.svg)](https://{{ cookiecutter.git_url }}/{{ cookiecutter.git_group }}/{{ cookiecutter.project_slug }}/-/commits/main)
[![Ruff](https://img.shields.io/endpoint?url=https://raw.githubusercontent.com/astral-sh/ruff/main/assets/badge/v2.json)](https://github.com/astral-sh/ruff)
[![security: bandit](https://img.shields.io/badge/security-bandit-yellow.svg)](https://github.com/PyCQA/bandit)


```bash
# Install pixi
curl -fsSL https://pixi.sh/install.sh | bash

# Install package and dependencies
pixi run install
```
